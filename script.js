var numNotas = 0
var total = 0

function verificar() {
    let nota = document.querySelector('input').value
    console.log(nota)
    if (nota == "") {
        alert("Por favor, insira uma nota.")
    }
    else {
        nota = parseFloat(nota)
        if (nota > 10 || nota < 0 || isNaN(nota)) {
            alert("A nota digitada é inválida, por favor, insira uma nota válida.")
        }
        else {
            adicionar(nota)
        }
    }
}

function adicionar(nota) {
    numNotas++

    let telaPrint = document.querySelector(".print")
    let content = document.createElement("p")
    content.innerText = ("A nota " + numNotas + " foi " + nota)
    telaPrint.append(content)
    
    total += nota
}

function calcular() {
    let output = document.querySelector(".outputP")
    output.innerText = ("A média é: " + (total / numNotas).toFixed(2))
}

let buttonAdd = document.querySelector(".btnAdicionar")
buttonAdd.addEventListener("click", () => { verificar() })

let buttonMedia = document.querySelector(".btnMedia")
buttonMedia.addEventListener("click", () => { calcular() })